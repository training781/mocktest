package com.amdocs.web.controller;
import static org.junit.Assert.*;
import org.junit.Test;
import  com.amdocs.Calculator;
import  com.amdocs.Increment;

public class CalculatorTest {


    @Test
    public void testAdd() throws Exception {

      final  int addResult= new Calculator().add();
        assertEquals("Add", 9, addResult);
        
    }
    @Test
    public void testSub() throws Exception {

      final  int result= new Calculator().sub();
        assertEquals("Sub", 3, result);

    }

@Test
    public void testDecreasecounter() throws Exception {

       final  int result= new Increment().decreasecounter(1);
        assertEquals("Counter", 1, result);

    }
@Test
    public void testDecreasecounterZero() throws Exception {

        final  int result= new Increment().decreasecounter(0);
        assertEquals("Counter", 0, result);

    }
@Test
    public void testDecreasecounterTwo() throws Exception {

        final  int result= new Increment().decreasecounter(2);
        assertEquals("Counter", 2, result);

    }

}
